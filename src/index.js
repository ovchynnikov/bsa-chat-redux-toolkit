import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom'
import { store } from './store/store';
import './index.css';
import App from './App';

const Application = (	
  <React.StrictMode>
	 <Router>
	   <Provider store={store}>
        <App />
	   </Provider>
	 </Router>
  </React.StrictMode>
);

ReactDOM.render(Application, document.getElementById('root'));
