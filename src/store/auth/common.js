const ActionType = {
    LOGIN: 'login',
    CLEAR_AUTH: 'CLEAR_AUTH'
};

export { ActionType };