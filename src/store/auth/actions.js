import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';


const loginUser = createAsyncThunk(ActionType.LOGIN, async(payload, { extra }) => ({
    user: await extra.authService.login(payload),
}));
const clearAuth = createAction(ActionType.CLEAR_AUTH);

export { loginUser, clearAuth };