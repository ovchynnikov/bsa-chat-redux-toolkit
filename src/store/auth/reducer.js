import { createReducer } from '@reduxjs/toolkit';
import { DataStatus } from '../../common/enums/enums';
import { loginUser, clearAuth } from './actions';


const initialState = {
    userLoggedIn: false,
    userRole: '',
    status: DataStatus.IDLE,
}

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(loginUser.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(loginUser.fulfilled, (state, { payload }) => {
        const { user } = payload;
        if (user.id) {
            state.userLoggedIn = true;
            state.userRole = user.role;
            state.status = DataStatus.SUCCESS;
        }
    });
    builder.addCase(loginUser.rejected, (state) => {
        state.status = DataStatus.ERROR;
    });
    builder.addCase(clearAuth, (state) => {
        state.status = 'idle';
    })

});

export { reducer };