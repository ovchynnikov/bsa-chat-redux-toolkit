const ActionType = {
    ADD: 'message/add',
    UPDATE: 'message/update',
    DELETE: 'message/delete',
};

export { ActionType };