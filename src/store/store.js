import { configureStore } from '@reduxjs/toolkit';
import { messages as messagesService } from '../services/services';
import { users as usersService } from '../services/services';
import { auth as authService } from '../services/services';
import { messages, users, auth } from './root-reducer';

const store = configureStore({
    reducer: {
        messages,
        users,
        auth
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            thunk: {
                extraArgument: {
                    messagesService,
                    usersService,
                    authService
                },
            },
        });
    },
});

export { store };