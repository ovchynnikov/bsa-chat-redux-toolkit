const ActionType = {
    FETCH_USERS: 'users',
    SET_EDIT_USER: 'SET_EDIT_USER',
    ADD: 'users/add',
    UPDATE: 'users/update',
    DELETE: 'users/delete',
};

export { ActionType };