import { createReducer } from '@reduxjs/toolkit';
import { DataStatus, ActionStatus } from '../../common/enums/enums';
import { fetchUsers, addUser, updateUser, deleteUser, setEditUser } from './actions';

const initialState = {
    users: [],
    status: DataStatus.IDLE,
    userOnEdit: {}
};

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchUsers.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchUsers.fulfilled, (state, { payload }) => {

        state.users = [...payload.users];
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(addUser.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(addUser.fulfilled, (state, { payload }) => {
        const { user } = payload;

        state.users = state.users.concat(user);
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(updateUser.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(updateUser.fulfilled, (state, { payload }) => {
        const user = payload;

        state.users = state.users.map((it) => {
            return it.id === user.id ? {...it, ...user } : it;
        });
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(deleteUser.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(deleteUser.fulfilled, (state, { payload }) => {
        const user = payload.user[0];

        state.users = state.users.filter((it) => it.id !== user.id);
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(setEditUser, (state, { payload }) => {
        state.userOnEdit = {...payload };
    })

    builder.addMatcher((action) => action.type.endsWith(ActionStatus.REJECTED), (state) => {
        state.status = DataStatus.ERROR;
    });
});

export { reducer };