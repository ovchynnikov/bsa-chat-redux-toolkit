import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { ActionType } from './common';

const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async(_args, { extra }) => ({
    users: await extra.usersService.getAll(),
}));

const addUser = createAsyncThunk(ActionType.ADD, async(payload, { extra }) => ({
    user: await extra.usersService.create(payload),
}))

const updateUser = createAsyncThunk(ActionType.UPDATE, async(payload, { extra }) => ({
    user: await extra.usersService.update(payload),
}));

const deleteUser = createAsyncThunk(ActionType.DELETE, async(id, { extra }) => ({
    user: await extra.usersService.delete(id)
}));

const setEditUser = createAction(ActionType.SET_EDIT_USER);

export { fetchUsers, addUser, updateUser, deleteUser, setEditUser };