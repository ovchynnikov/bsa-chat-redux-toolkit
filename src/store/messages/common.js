const ActionType = {
    FETCH_MESSAGES: 'messages',
    ADD: 'messages/add',
    UPDATE: 'messages/update',
    DELETE: 'messages/delete',
    SET_MESSAGES_AMOUNT: 'SET_MESSAGES_AMOUNT',
    CLEAR_INPUT: 'CLEAR_INPUT',
    SET_INPUT_VALUE: 'SET_INPUT_VALUE',
    SET_EDIT_TEXT: 'SET_EDIT_TEXT',
    SET_EDIT_MESSAGE: 'SET_EDIT_MESSAGE',
    SET_LAST_MESSAGE: 'SET_LAST_MESSAGE'
};

export { ActionType };