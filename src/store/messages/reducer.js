import { createReducer } from '@reduxjs/toolkit';

import { DataStatus } from '../../common/enums/enums';
import { fetchMessages, addMessage, updateMessage, deleteMessage, setMessagesAmount, clearCurrInput, setInputValue, setEditText, setEditMessage, setLastMessage } from './actions';


const initialState = {

    messages: [],
    status: DataStatus.IDLE,
    lastMessageDate: null,
    chatParticipants: 0,
    chatMessagesCounted: 0,
    currUser: {
        avatar: "https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ",
        user: "Helen",
        userId: "4b003c20-1b8f-11e8-9629-c7eca82aa7bd"
    },
    inputMode: 'create',
    currInputValue: '',
    lastUserMessage: {},
    editModal: false,
    messageOnEdit: {},
    preloader: true,
}

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchMessages.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
        const { messages } = payload;

        state.messages = messages;
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(addMessage.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(addMessage.fulfilled, (state, { payload }) => {
        const { message } = payload;

        state.messages = state.messages.concat(message);
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(updateMessage.fulfilled, (state, { payload }) => {
        const message = {...payload };

        state.messages = state.messages.map((it) => {
            return it.id === message.id ? {...it, ...message } : it;
        });
    });
    builder.addCase(deleteMessage.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(deleteMessage.fulfilled, (state, { payload }) => {
        const { id } = payload.message[0];
        state.messages = state.messages.filter((it) => it.id !== id);
        state.status = DataStatus.SUCCESS;
    });
    builder.addCase(setMessagesAmount, (state) => {
        state.chatMessagesCounted = state.messages.length;
    })
    builder.addCase(setInputValue, (state, { payload }) => {
        state.currInputValue = payload;
    })
    builder.addCase(clearCurrInput, (state) => {
        state.currInputValue = '';
    })
    builder.addCase(setEditMessage, (state, { payload }) => {
        state.messageOnEdit = {...payload };
    })
    builder.addCase(setEditText, (state, { payload }) => {
        state.messageOnEdit.text = payload;
    })
    builder.addCase(setLastMessage, (state) => {
        const lastMessage = {};
        const user = state.currUser.user;

        state.messages.forEach((item) => { if (item.user === user) lastMessage.last = {...item } });
        state.lastUserMessage = lastMessage.last;
    })
});


export { reducer };