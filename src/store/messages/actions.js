import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { ActionType } from './common';


const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async(_args, { extra }) => ({
    messages: await extra.messagesService.getAll(),
}));

const addMessage = createAsyncThunk(ActionType.ADD, async(payload, { extra }) => ({
    message: await extra.messagesService.create(payload),
}))

const updateMessage = createAsyncThunk(ActionType.UPDATE, async(payload, { extra }) => {
    const message = await extra.messagesService.update(payload)
    return message;
});

const deleteMessage = createAsyncThunk(ActionType.DELETE, async(payload, { extra }) => ({
    message: await extra.messagesService.delete(payload)
}));

const setMessagesAmount = createAction(ActionType.SET_MESSAGES_AMOUNT); // лишнее
const setInputValue = createAction(ActionType.SET_INPUT_VALUE);

const clearCurrInput = createAction(ActionType.CLEAR_INPUT);

const setEditMessage = createAction(ActionType.SET_EDIT_MESSAGE);
const setEditText = createAction(ActionType.SET_EDIT_TEXT);
const setLastMessage = createAction(ActionType.SET_LAST_MESSAGE);

export { fetchMessages, addMessage, updateMessage, deleteMessage, setMessagesAmount, setInputValue, clearCurrInput, setEditText, setEditMessage, setLastMessage };