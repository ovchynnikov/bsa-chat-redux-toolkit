const { message } = require('../models/message');
const { MessagesRepository } = require('../repositories/MessagesRepository');


const createMessageValid = (req, res, next) => {

    try {
        if (validateAllRows(req.body) &&
            req.body.text.length >= 1) {
            next();
        } else throw new Error(`Message entity to create is not valid`);
    } catch (error) {
        res.error = error;
        next();
    }
}

const messageObjectKeys = Object.keys(message);

function validateAllRows(incomeObject) {
    const incomeObjectKeys = Object.keys(incomeObject);

    function contains(messageObjectKeys, incomeObjectKeys) {
        if (incomeObjectKeys.length >= 1) {
            for (let i = 0; i < incomeObjectKeys.length; i++) {
                if (messageObjectKeys.indexOf(incomeObjectKeys[i]) === -1) return false;
            }
            return true;
        } else return false;
    }
    let result = contains(messageObjectKeys, incomeObjectKeys);

    return result;
}


const updateMessageValid = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        if (validateAllRows(req.body) && req.body.text.length > 0) {
            next();
        } else throw new Error(`Message entity to update is not valid`);

    } catch (error) {
        res.error = error;
        next();
    }
}

const validateBodyMiddlware = (req, res, next) => {
    try {
        if (req.body.hasOwnProperty("id")) {
            throw new Error(`You can't send id in request body`);
        } else next();
    } catch (error) {
        res.error = error;
        next();
    }
}

const messageDataExists = (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const id = req.body.id;
        const messageExists = MessagesRepository.getOne({ "id": id });
        if (messageExists) {
            next();
        } else throw new Error(`No messages with this id ${req.body.id} in database`);
    } catch (error) {
        res.error = error;
        next();
    }
}

exports.createMessageValid = createMessageValid;
exports.updateMessageValid = updateMessageValid;
exports.validateBodyMiddlware = validateBodyMiddlware;

exports.messageDataExists = messageDataExists;