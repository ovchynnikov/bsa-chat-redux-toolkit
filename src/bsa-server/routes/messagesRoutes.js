const { Router } = require('express');
const MessagesService = require('../services/messagesService.js');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createMessageValid, messageDataExists, validateBodyMiddlware, updateMessageValid } = require('../middlewares/message.validation.middleware');

const router = Router();

router.post('/', createMessageValid, validateBodyMiddlware, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const message = MessagesService.create(req.body);
            res.data = message;
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }
}, responseMiddleware)

router.get('/', (req, res, next) => {

    try {
        const messages = MessagesService.getAll();
        res.data = messages;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware)

router.get('/:id', validateBodyMiddlware, (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const message = MessagesService.search(req.params);
        res.data = message;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware)

router.put('/:id', messageDataExists, updateMessageValid, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const message = MessagesService.updateMessage(req.params.id, req.body);
            res.data = message;
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }

}, responseMiddleware);


router.delete('/:id', validateBodyMiddlware, (req, res, next) => {
    try {
        if (MessagesService.search(req.params)) {

            const deletedMessage = MessagesService.deleteMessage(req.params.id);
            if (deletedMessage.length > 0) {
                res.data = deletedMessage;
            }
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware);
module.exports = router;