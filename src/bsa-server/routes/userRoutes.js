const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, userDataExists, validateBodyMiddlware } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.post('/', createUserValid, userDataExists, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const user = UserService.create(req.body);
            if (user) {
                res.data = user;
            }
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }
}, responseMiddleware);

router.get('/', validateBodyMiddlware, (req, res, next) => {

    try {
        const users = UserService.getUsers();
        res.data = users;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', validateBodyMiddlware, (req, res, next) => {
    if (res.error) {
        next();
    }
    try {
        const user = UserService.search(req.params);
        res.data = user;
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    if (res.error) {
        next();
    } else {
        try {
            const userExists = UserService.search(req.params);
            if (!userExists) {
                throw new Error(`User entity to update is not valid`);
            } else {
                const user = UserService.updateUser(req.params.id, req.body);
                if (user) {
                    res.data = user;
                }
            }
        } catch (error) {
            res.error = error;
        } finally {
            next();
        }
    }

}, responseMiddleware);

router.delete('/:id', validateBodyMiddlware, (req, res, next) => {
    try {
        if (UserService.search(req.params)) {

            const deletedUser = UserService.deleteUser(req.params.id);
            if (deletedUser.length > 0) {
                res.data = deletedUser;
            }
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware);


module.exports = router;