const { BaseRepository } = require('./baseRepository');

class MessagesRepository extends BaseRepository {
    constructor() {
        super('messages');
    }
}

exports.MessagesRepository = new MessagesRepository();