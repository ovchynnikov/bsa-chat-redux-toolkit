const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    create(body) {

        const createdUser = UserRepository.create(body);
        if (!createdUser) {
            throw Error('User is not created');
        }
        return createdUser;
    }

    updateUser(id, dataToUpdate) {
        const updatedUser = UserRepository.update(id, dataToUpdate);
        if (!updatedUser) {
            throw Error('User is not updated');
        }
        return updatedUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item || item.length < 1) {
            throw Error('User not found');
        }
        return item;
    }

    getUsers() {
        const items = UserRepository.getAll();
        if (items.length < 1) {
            throw Error('Users not found');
        }
        return items;
    }

    deleteUser(user) {
        const deletedUser = UserRepository.delete(user);
        if (!deletedUser) {
            throw Error('User not found');
        }
        return deletedUser;
    }

}

module.exports = new UserService();