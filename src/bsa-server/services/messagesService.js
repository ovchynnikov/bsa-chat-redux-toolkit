const { MessagesRepository } = require('../repositories/messagesRepository');

class MessagesService {

    create(body) {
        const createdMessage = MessagesRepository.create(body);
        if (!createdMessage) {
            throw Error('Message is not created');
        }
        return createdMessage;
    }

    getAll() {
        const items = MessagesRepository.getAll();
        if (items.length < 1) {
            throw Error('Messages are not found');
        }
        return items;
    }

    search(search) {
        const item = MessagesRepository.getOne(search);
        if (!item || item.length < 1) {
            throw Error('Message not found');
        }
        return item;
    }

    updateMessage(id, dataToUpdate) {
        const updatedMessage = MessagesRepository.update(id, dataToUpdate);
        if (!updatedMessage) {
            throw Error('Message is not updated');
        }
        return updatedMessage;
    }

    deleteMessage(message) {
        const deletedMessage = MessagesRepository.delete(message);
        if (!deletedMessage) {
            throw Error('Message not found');
        }
        return deletedMessage;
    }
}

module.exports = new MessagesService();