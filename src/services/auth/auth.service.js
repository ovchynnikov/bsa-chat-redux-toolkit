import { ApiPath, ContentType, HttpMethod } from '../../common/enums/enums';

class Auth {
    constructor({ baseUrl, http }) {
        this._baseUrl = baseUrl;
        this._http = http;
        this._basePath = ApiPath.LOGIN;
    }

    login(credentials) {
        return this._http.load(this._getUrl(), {
            method: HttpMethod.POST,
            contentType: ContentType.JSON,
            payload: JSON.stringify(credentials),
        });
    }
    _getUrl(path = '') {
        return `${this._baseUrl}${this._basePath}/${path}`;
    }
}

export { Auth };