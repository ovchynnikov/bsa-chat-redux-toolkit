import React from 'react';
import './App.css';
import Chat from './components/Chat/Chat';
import {Redirect, Route, Switch} from 'react-router-dom';
import Login from './components/Login/Login';
import UsersList from './components/UsersList/UsersList';
import PageHeader from './components/HeaderApp/header';
import MessageEditor from './components/MessageEditor/MessageEditor';
import UserEditor from './components/UserEditor/UserEditor';


function App() {

		return (
			<div className="App">
				<PageHeader />
			<Switch>
			<Route exact path="/">
					<Redirect to={'/login'} />
				</Route>
				<Route exact path="/login">
					<Login />
				</Route>
				<Route exact path="/chat">
      		<Chat/>
			  </Route>
				<Route exact path="/users-list"> 
					<UsersList/>
				</Route>
				<Route exact path="/users-list/:id"> 
					<UserEditor/>
				</Route>
				<Route exact path="/users-list/create"> 
					<UserEditor/>
				</Route>
				<Route path="/message/:id"> 
					<MessageEditor/>
				</Route>
      </Switch>
    </div>
  );

}

export default App;
