const DataPlaceholder = {
    NO_MESSAGES: 'There are no messages 🥥',
    NO_USERS: 'Opps... there is no users',
    PAGE_NOT_FOUND: 'Oops... page not found',
};

export { DataPlaceholder };