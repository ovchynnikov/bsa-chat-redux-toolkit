const REACT_APP_SERVER_URL = 'http://localhost:3050/api';

const ENV = {
    API: {
        URL: REACT_APP_SERVER_URL,
    },
};

export { ENV };