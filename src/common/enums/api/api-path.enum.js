const ApiPath = {
    MESSAGES: '/messages',
    USERS: '/users',
    LOGIN: '/auth/login'
};

export { ApiPath };