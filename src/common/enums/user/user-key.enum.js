const UserKey = {
    ROLE: 'role',
    ID: 'id',
    FIRST_NAME: 'firstName',
    LAST_NAME: 'lastName',
    EMAIL: 'email',
    PASSWORD: 'password',
};

export { UserKey };