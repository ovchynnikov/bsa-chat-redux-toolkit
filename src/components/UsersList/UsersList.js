import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import UserItem from '../UserItem/UserItem';
import { users as usersActionCreator } from '../../store/actions';
import Preloader from '../Preloader/Preloader';
import { DataStatus } from '../../common/enums/enums';
import './UserList.css';

const UsersList = () => {
	const history = useHistory();
	const dispatch = useDispatch();

	useEffect(() => {
    dispatch(usersActionCreator.fetchUsers());
  }, [dispatch]);

	const { userLoggedIn, userRole, users, status, authStatus } = useSelector(({ auth, users }) => ({
    userLoggedIn: auth.userLoggedIn,
    userRole: auth.userRole,
		users: users.users,
		userOnEdit: users.userOnEdit,
		status: users.status,
		authStatus: auth.status
  }));
	
	const onDelete = (id) => {
		dispatch(usersActionCreator.deleteUser(id));
	}

	const getUser = id => {
		let oldUser = users.find(item => item.id === id);
		return oldUser;
	}

	const onEdit = (id) => {
		const selectedUser = getUser(id);
		dispatch(usersActionCreator.setEditUser(selectedUser));
		history.push(`/users-list/${id}`)
	}

	const onCreate = () => {
		console.log('onCreate');
		history.push(`/users-list/create`)
	}

	const checkIfUserAdmin = () => {
		if(status === DataStatus.PENDING || authStatus === DataStatus.PENDING) {
			return <Preloader />
		} 
		if (userLoggedIn && userRole === 'admin' && status === DataStatus.SUCCESS) {
			return 	(
				<div className="login-wrapper">
				<h1>This is UsersList page</h1>
				{
						users.map(user => {
							return (
								<UserItem
									key={user.id}
									id={user.id}
									name={user.firstName}
									surname={user.lastName}
									email={user.email}
									onEdit={onEdit}
									onDelete={onDelete}
								/>
							);
						})
					}
					<button className="btn-create-user" onClick={onCreate}>Create User</button>
			</div>
			)
		}
		if(status === DataStatus.ERROR){
			  return <h2>ERROR fetching Users...</h2>
			
		} 
		if(userLoggedIn && userRole === 'user' && status === DataStatus.SUCCESS) {
		 	  history.push("/chat")
				 return null
				} else {
			history.push("/chat")
			return null
		}
	}
	
	return checkIfUserAdmin();
}

export default UsersList;