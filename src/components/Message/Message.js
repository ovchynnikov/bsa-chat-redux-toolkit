import React from 'react';
import './Message.css';

class Message extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			liked: false
		};
	this.likeToggle = this.likeToggle.bind(this);
	}

	likeToggle() {
		this.setState(state => {
			return {
			...state,
			liked: !this.state.liked
			}
		})

	}
	render() {
    return (
        <div className="message" id={this.props.message.id}>
            
						<div className="user-wrapper">
							<img src={this.props.message.avatar} alt="user avatar" className="message-user-avatar"></img>
							<span className="message-user-name">{this.props.message.user}</span>
							<span className="time-span message-time">{this.props.time}</span>
						</div>
						<p className="message-text">{this.props.message.text}</p>
						<div className="button-wrapper">
							 
						{this.state.liked ? <p className="heart">1 &#10084;</p> : null}  
						<button className={this.state.liked ? "message-liked" : "message-like"} onClick={this.likeToggle}>{this.state.liked ? "Unlike" : "Like"}</button>
						</div>
        </div>
    )
  }
}
export default Message;