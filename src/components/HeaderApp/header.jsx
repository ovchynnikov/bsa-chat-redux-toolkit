import { Link } from "react-router-dom";
import './style.css';

const PageHeader = () => (
  <header className="page-header">
    <div className="page-header__wrapper">
      <h1 className="page-header__title">
        <Link to={'/chat'}>Chat</Link>
        <Link to={'/users-list'}>Users Link</Link>
        <Link to={'/login'}>Login</Link>
      </h1>
    </div>
  </header>
);

export default PageHeader;
