import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { auth as authActionCreator } from '../../store/actions';
import './Login.css';
import { useHistory } from 'react-router-dom';
import { DataStatus } from '../../common/enums/enums';
import Preloader from '../Preloader/Preloader';


 export default function Login() {
	const history = useHistory();
  const dispatch = useDispatch();

  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

	const { status } = useSelector(({ auth }) => ({
    status: auth.status,
	
  }));

	const clearAuthStatus = () => {
		dispatch(authActionCreator.clearAuth());
		}

	const handleSubmit = e => {
		e.preventDefault();
		dispatch(authActionCreator.loginUser({
			userName,
			password
		}));
			setTimeout(() => {
				history.push("/users-list");
			}, 500);
	};
	
		if (status === DataStatus.PENDING) return <Preloader />
		if (status === DataStatus.ERROR) return (
				<>
				  <h1 className="user-not-found">Sorry, User not found</h1>
				  <button onClick={clearAuthStatus}>Go to login without page refresh</button>
				</>
		)

		
			return(
				<div className="login-wrapper">
          <h1>Please Log In</h1>
          <form onSubmit={handleSubmit}>
            <label>
              <p>Username</p>
              <input type="text" onChange={e => setUserName(e.target.value)} />
            </label>
            <label>
              <p>Password</p>
              <input type="password" onChange={e => setPassword(e.target.value)} />
            </label>
            <div>
              <button type="submit">Submit</button>
            </div>
          </form>
        </div>
      )
}