import React from "react";
import { connect } from 'react-redux';
import UserItem from './UserItem/UserItem';
import * as actions from './actions';
import { setCurrentUserId, showPage } from '../userPage/actions';

const UsersEditor = (props) => {
	

	function onEdit(id) {
		props.setCurrentUserId(id);
		this.props.showPage();
	}

	function onDelete(id) {
		props.deleteUser(id);
	}

	function onAdd() {
		props.showPage();
	}

		return (
			<div >
				<div >
					{
						props.users.map(user => {
							return (
								<UserItem
									key={user.id}
									id={user.id}
									name={user.name}
									surname={user.surname}
									email={user.email}
									onEdit={onEdit}
									onDelete={onDelete}
								/>
							);
						})
					}
				</div>
				<div >
					<button
						// className="btn btn-success"
						onClick={onAdd}
						style={{ margin: "5px" }}
					>
						Add user
					</button>
				</div>
			</div>
		);
	
}

const mapStateToProps = (state) => {
	return {
		users: state.users
	}
};

const mapDispatchToProps = {
	...actions,
	setCurrentUserId,
	showPage
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersEditor);