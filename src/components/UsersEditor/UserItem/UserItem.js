import React from "react";

export const UserItem = (props) => {
        const { id, name, surname, email } = this.props;
        return (
            <div className="container list-group-item">
                <div className="row">
                    <div className="col-8">
                        <span className=" float-left" style={{ fontSize: "2em", margin: "2px" }}>{name} {surname}</span>
                        <span c style={{ fontSize: "2em", margin: "2px" }}>{email}</span>
                    </div>
                    <div className="col-4 btn-group">
                        <button  onClick={(e) => props.onEdit(id)}> Edit </button>
                        <button  onClick={(e) => props.onDelete(id)}> Delete </button>
                    </div>
                </div>
            </div>
        );
};