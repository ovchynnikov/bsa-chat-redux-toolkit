import React from "react";
import './UserItem.css';

const UserItem = (props) => {
	const { id, name, surname, email } = props;

	return (
		<div className="user-container">
						<div className="">
								<span className="" style={{ fontSize: "2em", margin: "2px" }}>{name}   {surname}</span>
								<span className="email-span" style={{ fontSize: "1.5em", margin: "2px" }}>  {email}</span>
						</div>
						<div className="">
								<button className="btn" onClick={(e) => props.onEdit(id)}> Edit </button>
								<button className="btn" onClick={(e) => props.onDelete(id)}> Delete </button>
						</div>
		</div>
 );
}
export default UserItem;
