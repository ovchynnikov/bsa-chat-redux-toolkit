import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../Header/Header';
import MessageInput from '../MessageInput/MessageInput';
import MessageList from '../MessageList/MessageList';
import Preloader from '../Preloader/Preloader';
import { messages as messagesActionCreator } from '../../store/actions';
import { DataStatus } from '../../common/enums/enums';
import { Redirect, useHistory } from "react-router-dom";

const Chat = (props) => {
	const history = useHistory();

  const { messages, status, currUser, userLoggedIn, currInputValue, inputMode, lastMessage } = useSelector(({ messages, auth }) => ({
    messages: messages.messages,
		lastMessage: messages.lastUserMessage,
    status: messages.status,
		currInputValue: messages.currInputValue,
		inputMode: messages.inputMode,
		currUser: messages.currUser,
		userLoggedIn: auth.userLoggedIn
  }));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(messagesActionCreator.fetchMessages());
    dispatch(messagesActionCreator.setMessagesAmount());
		
  }, [dispatch]);

useEffect(() => {
	dispatch(messagesActionCreator.setLastMessage());

}, [dispatch, messages])


		const countParticipants = () => {
		const resArr = [];
		messages.forEach(function(item) {
      let i = resArr.findIndex(x => x.user === item.user);
       if(i <= -1) {
         resArr.push({id: item.userId, user: item.user});
       }
  })
		return resArr.length;
	}

	const getLastMessageDate = () => {
		if(messages.length > 0){

			let lastDate =  new Date(messages[messages.length-1].createdAt);
			let dd = lastDate.getDate();
			if (dd < 10) dd = '0' + dd;
			
			let mm = lastDate.getMonth() + 1;
			if (mm < 10) mm = '0' + mm;
			
			let yy = lastDate.getFullYear();
			if (yy < 10) yy = '0' + yy;
			
			let hrs = lastDate.getHours();
			if (hrs < 10) hrs = '0' + hrs;
			
			let mins = lastDate.getMinutes();
			if (mins < 10) mins = '0' + mins;
			let lastMsgDate = dd + '.' + mm + '.' + yy + ' ' + hrs + ':' + mins;
			
			return lastMsgDate;
		}
	}

	const handleInput = (e) => {
		dispatch(messagesActionCreator.setInputValue(e.target.value));
	}

	const onSendMessage = (event) => {
		event.preventDefault();
		
		let { avatar, user, userId } = currUser;
		if(inputMode === 'create' && currInputValue.trim().length > 0) {

			let newMessage = {
				avatar: avatar,
				editedAt: "",
				text: currInputValue.trim(),
				user: user,
				userId: userId
			}
			dispatch(messagesActionCreator.addMessage(newMessage));
			dispatch(messagesActionCreator.clearCurrInput());
		} 
	}
	
	const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      onSendMessage(e);
    } if (e.keyCode === 38) {
			const idToEdit = lastMessage.id;
		  let currMessage = getMessageInfo(idToEdit);
		  dispatch(messagesActionCreator.setEditMessage(currMessage));

		  history.push(`/message/${idToEdit}`)

		}
  }

	const handleDelete = (event) => {
		dispatch(messagesActionCreator.deleteMessage(event.target.id));
	}

	const getMessageInfo = id => {
		let oldMessage = messages.find(item => item.id === id);
		return oldMessage;
	}

	const setEditMessageInState = event => {
		const idToEdit = event.target.dataset.editid; 
		let currMessage = getMessageInfo(idToEdit);
		dispatch(messagesActionCreator.setEditMessage(currMessage));
		
		history.push(`/message/${idToEdit}`)
	}


	
		if(status === DataStatus.PENDING) {
			return <Preloader />
		} else if (!userLoggedIn) {
			 return <Redirect to="/login"/>
		}
			  return 	(<div className="chat">

							  <Header 
							  chatParticipants={countParticipants()} 
								countedMessages={messages.length} 
								lastMessageDate={getLastMessageDate()}
								/>

              	<MessageList 
								messages={messages} 
								currUser={currUser} 
								handleDelete={handleDelete} 
								setEditMessage={setEditMessageInState}
								/>

							  <MessageInput 
							  onChange={handleInput} 
							  onSubmitHandler={onSendMessage} 
							  value={currInputValue}
							  mode={inputMode}
							  onKeyDown={handleKeyDown}
							  />
						</div>)
}

export default Chat;