import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { messages as messagesActionCreator } from '../../store/actions';
import './MessageEditor.css';

function MessageEditor() {
	let { id } = useParams();

	const dispatch = useDispatch();
	const history = useHistory();

	const { message } = useSelector(({ messages }) => ({
		message: messages.messageOnEdit,
  }));

	const hanldeInput = (event) => {
		dispatch(messagesActionCreator.setEditText(event.target.value))
	}

	const onClose = () => {
		history.push('/chat')
	}

	const handleSave = () => {
		const updateMessage = {...message};
		dispatch(messagesActionCreator.updateMessage({...updateMessage}));
		history.push('/chat');
	}

	return (
		<>
			<div className={`edit-message-container`}>
					<div className="edit-body">
							<h3>Edit the message {id}</h3>
							<textarea className="edit-message-input" value={message.text} onChange={hanldeInput}></textarea> 
							<button className="edit-button edit-message-close" onClick={onClose}>Cancel</button>
							<button className="edit-button edit-message-button" onClick={handleSave}>Save</button>
					</div>
			</div>
		</>
)
}

export default MessageEditor;

