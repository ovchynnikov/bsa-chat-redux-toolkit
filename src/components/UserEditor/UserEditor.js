import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { DataStatus } from '../../common/enums/enums';
import { users as usersActionCreator } from '../../store/actions';
import Preloader from '../Preloader/Preloader';
import './UserEditor.css';

function UserEditor() {
	let { id } = useParams();

	const { user, userRole, status } = useSelector(({ auth, users }) => ({
		user: users.userOnEdit,
		userRole: auth.userRole,
		status: users.status
  }));

	const [firstName, setFirstName] = useState(user.firstName);
	const [lastName, setLastName] = useState(user.lastName);
	const [email, setEmail] = useState(user.email);
	const [password, setPassword] = useState(user.password);
	const [userName, setUserName] = useState(user.userName)

	const dispatch = useDispatch();
	const history = useHistory();

	const hanldeInput = (event) => {
		const inputName = event.target.className;
    switch (inputName) {
      case 'input edit-user-firstname':
    		setFirstName(event.target.value)
        break;
      case 'input edit-user-lastname':
        setLastName(event.target.value)
        break;
      case 'input edit-user-email':
        setEmail(event.target.value)
        break;
			case 'input edit-user-password':
				setPassword(event.target.value)
				break;
			case 'input edit-user-name':
				setUserName(event.target.value)
				break;
      default:
        console.log(`Sorry, this ${inputName} not found in switch.`);
    }
	}

	const onClose = () => {
		history.push('/users-list')
	}

	const clearLocalData = () => {
		setFirstName('')
    setLastName('')
    setEmail('')
		setPassword('')
		setUserName('')
	}
	const handleSave = () => {
		if(user.id && id) {
			const updatedUser = {...user, userName, firstName, lastName, email, password};
			dispatch(usersActionCreator.updateUser({...updatedUser}));
			history.push('/users-list');
			clearLocalData();
		} else {
			const newUser = {userName, firstName, lastName, email, password, role: 'user'};
			dispatch(usersActionCreator.addUser({...newUser}));
			history.push('/users-list');
			clearLocalData();
	    }
		}

		if (status === DataStatus.PENDING) {
			return <Preloader />
		}
		if (userRole === 'admin' &&  DataStatus.SUCCESS) {

			return (
			  <>
			    <div className={`edit-user-container`}>
			    		<div>
			    				<h3>Edit User {user.firstName} {user.lastName}</h3>
			    				<label>User Name</label>
			    				<textarea className="input edit-user-name" value={userName} onChange={hanldeInput}></textarea>
			    				<label>Password</label>
			    				<textarea className="input edit-user-password" value={password} onChange={hanldeInput}></textarea>
			    				<label>First Name</label>
			    				<textarea className="input edit-user-firstname" value={firstName} onChange={hanldeInput}></textarea>
			    				<label>Last Name</label>
			    				<textarea className="input edit-user-lastname" value={lastName} onChange={hanldeInput}></textarea> 
			    				<label>Email</label>
			    				<textarea className="input edit-user-email" value={email} onChange={hanldeInput}></textarea>
			    				
			    				<button className="user-edit-btn edit-user-close" onClick={onClose}>Cancel</button>
			    				<button className="user-edit-btn edit-user-button" onClick={handleSave}>Save</button>
			    		</div>
			    </div>
			  </>
  		)
	} else if (userRole === 'user' &&  DataStatus.SUCCESS){
		history.push("/chat");
		return null
	} else {
		history.push("/chat");
		return null
	}
}

export default UserEditor;

