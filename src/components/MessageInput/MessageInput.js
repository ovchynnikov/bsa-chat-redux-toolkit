import React from 'react';
import './MessageInput.css';

const MessageInput = props => {
	
    return (
        <div className="message-input">
					<form onSubmit={props.onSubmitHandler}>
            <textarea
									 onKeyDown={props.onKeyDown}
									 placeholder="type a message"
						       rows="5"
									 className="message-input-text"
                   value={props.value}
                   onChange={props.onChange}
            />
						<input className="message-input-button" type="submit" value='Send'></input>
						</form>
        </div>
    )
}

export default MessageInput;